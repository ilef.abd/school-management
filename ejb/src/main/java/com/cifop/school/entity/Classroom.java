package com.cifop.school.entity;



import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


@Entity
@Table(name="Class")
public class Classroom implements Serializable{
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String grade;
	private String name ;
	@OneToMany(fetch = FetchType.EAGER)
	private List<Student> students = new LinkedList<Student>();
	public Classroom(long id, String grade, String name, List<Student> students) {
		super();
		this.id = id;
		this.grade = grade;
		this.name = name;
		this.students = students;
	}
	public Classroom() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	
	}
	


