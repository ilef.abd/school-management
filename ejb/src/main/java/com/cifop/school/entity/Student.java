package com.cifop.school.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;


//@NamedQuery(
//		name="findAllStudentByName"
//		query="SELECT s From Student s )
@Entity
public class Student implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id ;
	private String firstName;
	private String lastName ;
	private String adress ;
	private Date dateOfBirthday ;
	@ManyToOne
	@JoinColumn(name="StudentId")
	private Classroom classroom ;
	
	
	
	public Student() {
		super();
	}



	public Student(String firstName, String lastName, String adress, Date dateOfBirthday, Classroom classroom) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.dateOfBirthday = dateOfBirthday;
		this.classroom = classroom;
	}
	
	
}
