package com.cifop.school.service.soap;

import java.util.List;

import com.cifop.school.entity.Student;
import com.cifop.school.service.StudentService;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.jws.WebService;

@Stateless
@WebService
public class StudentSoapWebServiceImplementation implements StudentSoapWebService{

	@EJB
	StudentService studentService ;
	@Override
	public Student findStudentById(long id) {
		// TODO Auto-generated method stub
		return  studentService.findStudentByID(id);
	}

	@Override
	public List<Student> findListStudents() {
		// TODO Auto-generated method stub
		return studentService.findStudents();
	}

}
