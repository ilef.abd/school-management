package com.cifop.school.service;

import java.util.List;

import com.cifop.school.entity.Classroom;

import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateful
public class ClassroomService {
	
	
	@PersistenceContext
	EntityManager entitymanager ;
	
	
	public void saveClassroom(Classroom classroom)
	{
		entitymanager.persist(classroom);
		}

    /**
     * 
     * @param classRoom
     */
    public void updateClassRoom(Classroom classRoom) {
    	entitymanager.merge(classRoom);
    }

    /**
     * 
     * @param classRoom
     */
    public void removeClassRoom(Classroom classRoom) {
    	entitymanager.remove(classRoom);
    }

    /**
     * 
     * @param id
     * @return
     */
    public Classroom findClassRoomById(long id) {
        return entitymanager.find(Classroom.class, id);
    }

    public List<Classroom> findAllClassRoom() {
        return  entitymanager
                .createQuery("select c from Classroom c")
                .getResultList();
}
}