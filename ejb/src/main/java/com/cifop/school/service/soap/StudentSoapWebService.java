package com.cifop.school.service.soap;

import java.util.List;

import com.cifop.school.entity.Student;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;

@WebService
public interface StudentSoapWebService  {
	@WebMethod
	public Student findStudentById(@WebParam(name = "id",targetNamespace = "http://soap.service.school.cifop.com/") long id);
	@WebMethod
	public List<Student> findListStudents() ;
}
