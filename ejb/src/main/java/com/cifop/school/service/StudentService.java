package com.cifop.school.service;

import java.util.List;

import com.cifop.school.entity.Classroom;
import com.cifop.school.entity.Student;

import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateful
public class StudentService {
	
	@PersistenceContext
	private EntityManager entityManager ;
	
	public void saveStudent(Student student) {
		entityManager.persist(student);
		 
	}
	public void deleteStudent(Student student) {
		entityManager.remove(student);
	}
	
	public Student findStudentByID(long id)
	{
		return entityManager.find(Student.class, id);
	}
	
	public Student updateStudent(Student student)
	{
		return entityManager.merge(student);
	}
	
	public List<Student> findStudents()
	{
		//create Query
		return entityManager.createQuery("SELECT student FROM Student student").getResultList(); //jpql
	}
	
	public List<Student> findStudentsByClassroom(String name)
	{
		return entityManager.
				createQuery("SELECT s FROM  Classroom c join c.students s where c.name:name").setParameter("name",name).getResultList();
	}
	
	public Student findStudentById(long id)
	{
		
		return (Student) entityManager.createQuery("SELECT student FROM Student student where student.id:id").setParameter("id", id).getSingleResult();
	}
}
