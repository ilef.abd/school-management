package com.cifop.school.resources.classroom;

import java.util.List;

import com.cifop.school.entity.Classroom;
import com.cifop.school.service.ClassroomService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/classrooms")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClassRoomRessource {
	
	@Inject
	ClassroomService classRoomService;
	
	@GET
    public List<Classroom> listClassRooms() {
        return classRoomService.findAllClassRoom();
    }
    
    @GET
    @Path("/{id}")
    public Classroom findClassRoomById(@PathParam("id") long id) {
        return classRoomService.findClassRoomById(id);
}
    @POST
    public Classroom saveClassRoom(Classroom classroom)
    {
        classRoomService.saveClassroom(classroom);
        return classroom;
    }
}