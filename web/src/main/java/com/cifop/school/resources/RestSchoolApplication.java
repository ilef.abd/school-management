package com.cifop.school.resources;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
@ApplicationPath("/api")
public class RestSchoolApplication extends Application {

}
