package com.cifop.school.controller;

import com.cifop.school.entity.Classroom;
import com.cifop.school.service.ClassroomService;

import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
@RequestScoped
public class ClassroomController {
	
	@Inject
	ClassroomService classroomService ;
	@Inject
	FacesContext facesContext;
	Classroom classroom = new Classroom();
	
	public void doCreateClassroom() {
		classroomService.saveClassroom(classroom);
		facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                "Class is saved successfully with id " + classroom.getId()) );
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
}
